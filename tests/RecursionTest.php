<?php
/**
 * Created by PhpStorm.
 * User: Noa
 * Date: 06/06/2018
 * Time: 20:58
 */

use Noa\Lab\Recursion;

class RecursionTest extends \PHPUnit\Framework\TestCase
{

    public function testTransform()
    {
        $instance = new Recursion();

        $input = ["test", "test", "12345"];

        $expected = [
            "test" => [
                "test" => "12345"
            ]
        ];

        $result = $instance->transform($input);

        $this->assertEquals($expected, $result);
    }

    public function testTransformDeeper() {

        $instance = new Recursion();

        $input = ["test1", "test2", "test3", "test4", "12345"];

        $expected = [
            "test1" => [
                "test2" => [
                    "test3" => [
                        "test4" => "12345"
                    ]
                ]
            ]
        ];

        $result = $instance->transform($input);

        $this->assertEquals($expected, $result);
    }
}
