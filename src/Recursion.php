<?php
/**
 * Created by PhpStorm.
 * User: Noa
 * Date: 06/06/2018
 * Time: 20:50
 */

namespace Noa\Lab;


class Recursion
{
    public function transform($input) : array {

        $output = null;

        do {

            $data = array_pop($input);
            if (!is_null($output)) {
                $outputTmp = [];
                $outputTmp[$data] = $output;
                $output = $outputTmp;
            } else {
                $output = $data;
            }

        } while(count($input));

        return $output;
    }
}